package main //определением пакета

import "fmt" //import позволяет подключить сторонние пакеты fmt (сокращение от format) 
//Использование двойных кавычек называется «строковым литералом»

func main(){
var str string
str=" Pavel" 
	fmt.Println("Hello my name is"+str) //Print line
}

/*
Создать новую исполняемую программу, которая использует библиотеку fmt
 и содержит функцию main. Эта функция не имеет аргументов,
 ничего не возвращает и делает следующее: использует функцию Println из библиотеки fmt
 и вызывает её, передавая один аргумент — строку Hello World.
*/