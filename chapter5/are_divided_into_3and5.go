package main
import "fmt"
func main(){
	for i:=0; i<=100;i++{
		if (i%3==0) && (i%5==0){ 
			fmt.Print("FizzBuzz - ")
			fmt.Print(i)
			fmt.Println()
		} else if i%3==0 { 
			fmt.Print("Fizz - ")
			fmt.Print(i)
			fmt.Println()
		} else if i%5==0 {
			fmt.Print("Buzz - ")
			fmt.Print(i)
			fmt.Println()
		}
	}
}
