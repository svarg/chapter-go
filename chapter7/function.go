package main

import "fmt"

func exampleAverage(xs []float64) float64 {
	total := 0.0
	for _, v := range xs {
		fmt.Println(v)
		total += v
	}
	fmt.Println(total)
	return total / float64(len(xs))
	//panic ("Not Implemented")
}
func exampleMultipleValues(i int) (int, int) {
	squard := i * i
	sum := i + i
	return squard, sum
}
func total(args ...int) int {
	fmt.Println("total:")
	//fmt.Printf("",args)
	total := 0
	for _, v := range args {
		total += v
	}
	return total
}
func sum(args []int) int {
	total := 0

	for _, inc := range args {
		total += inc
		fmt.Println( inc)
	}
	return total
}
func half(num int) bool{
	if num%2==0 {
		return true
	}else {
		return false
	}
}
func max(args ...int)int{
	maxNum:=0
	for _,num:=range args{
		if num>maxNum {
			maxNum = num
		}
	}
	return  maxNum
}
func makeOddGenerator () func() uint{
	i:= uint(1)
	return func() (ret uint) {
		ret = i
		i+=2
		return
	}

}
func main() {
	//xs := []float64{99, 98, 97, 96}
	//fmt.Println(exampleAverage(xs))
	//x, y := exampleMultipleValues(5)
	//fmt.Println(x)
	//fmt.Println(y)
	//totalValue:=total(1,2,3,4,5)
	//fmt.Println(totalValue)

	fmt.Println(sum([]int{11, 23, 412, 12}))
	fmt.Println(half(4))
	fmt.Println(max(23,12,44,123,11,23,42))
	odd:=makeOddGenerator();
	fmt.Println(odd())

}
